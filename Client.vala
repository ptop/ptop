/* Client.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public class GeneralInfo {
	public bool success { get; set; }

	public string host { get; set; }
	public uint16 port { get; set; }

	public uint16 local_server_port { get; set; }
}

public class DownloadInfo : GeneralInfo {
	public string local_path { get; set; }
	public string package_name { get; set; }
	public string filename { get; set; }
	public int chunk_number { get; set; }

	public int64 position_offset { get; set; }

	public string checksum { get; set; }
}

public class Client : Object {
	SocketClient client_socket = new SocketClient ();
	public signal void download_finnished (DownloadInfo info);

	long download_limit { get; set; }

	ThreadPool download_thread_pool;
	AsyncQueue<DownloadInfo> download_finnish_queue;
	AsyncQueue<DownloadInfo> download_request_queue;

	Mutex mutex;


	public Client () throws ThreadError {
		download_limit = 100 * 1024;
		mutex = new Mutex ();

		download_thread_pool = new ThreadPool (download_request_executor, 5, true);
		download_finnish_queue = new AsyncQueue<DownloadInfo> ();
		download_request_queue = new AsyncQueue<DownloadInfo> ();
		Timeout.add (3000, on_check_downloads_finnished);
	}

	Response get_headers (DataInputStream stream) throws Error {
		var response = new Response ();
		response.read_header (stream);
		return response;
	}

	public void process_queues () {
		download_thread_pool.push ((void*)1);
	}

	public void add_to_queue (DownloadInfo download_info) {
		download_request_queue.push (download_info);
	}

	void download_request_executor (void* parameter) {
		if (download_request_queue.length () > 0) {
			var download_info = download_request_queue.pop ();

			try {
				print ("Downloading: %s - %s\n", download_info.package_name, download_info.filename);
				string checksum = request_file (download_info);

				print ("Downloaded: %s - %s checksum %s\n", download_info.package_name, download_info.filename, checksum);

				download_info.checksum = checksum;
				download_info.success = true;

				download_finnish_queue.push (download_info);
			} catch (Error error) {
				print ("Error in download thread: %s\n", error.message);
				download_info.success = false;
				download_finnish_queue.push (download_info);
			}
		}
	}

	bool on_check_downloads_finnished () {
		while (download_finnish_queue.length () > 0) {
			DownloadInfo download_info = download_finnish_queue.pop ();
			download_finnished (download_info);
		}
		process_queues ();
		return true;
	}

	public string request_file (DownloadInfo info) throws Error {
		var connection = get_connection (info.host, info.port);

		var output_data_stream = new DataOutputStream (connection.output_stream);
		var data_input_stream = new DataInputStream (new ThrottledInputStream (connection.input_stream, download_limit));

		var request = new Request ();
		request.method = "GET";
		request.resource = "/get/%s/%s".printf (info.package_name, info.filename);
		request.headers.set ("Chunk", info.chunk_number.to_string ());

		request.write (output_data_stream);

		var response = get_headers (data_input_stream);
		if (response.status_code != 200) {
			throw new HttpError.FILE_NOT_FOUND ("Response returned status code: %d %s".printf (response.status_code, response.reason));
		}

		var checksum = new Checksum (ChecksumType.SHA1);

		size_t read_count = 0;
		size_t write_count = 0;
		uchar[] buffer = new uchar[64*1024];
		bool result = false;
		var file = File.new_for_path (info.local_path + "/" + info.package_name + "/" + info.filename);
		if (!file.query_exists ()) {
			file.create (FileCreateFlags.NONE);
		}
		var file_io_stream = file.open_readwrite ();
		var file_output_stream = file_io_stream.output_stream;
		file_io_stream.seek (info.position_offset, SeekType.SET);
		do {
			result = data_input_stream.read_all (buffer, 64*1024, out read_count);
			file_output_stream.write_all (buffer, read_count, out write_count);
			checksum.update (buffer, read_count);
		} while (result && read_count > 0);
		file_output_stream.flush ();
		file_output_stream.close ();
		return checksum.get_string ();
	}

	SocketConnection? get_connection (string host, uint16 port) throws Error {
		mutex.lock ();
		var resolver = Resolver.get_default ();
		var addresses = resolver.lookup_by_name (host);
		var address = addresses.nth_data (0);

		SocketConnection? connection = client_socket.connect (new InetSocketAddress (address, port));
		mutex.unlock ();
		return connection;
	}

	public string request_package_metadata (string uri) throws Error {
		var parsed_uri = new Soup.URI (uri);
		string path = parsed_uri.path;

		var connection = get_connection (parsed_uri.host, (uint16) parsed_uri.port);

		var output_data_stream = new DataOutputStream (connection.output_stream);
		var input_data_stream = new DataInputStream (connection.input_stream);

		var request = new Request ();
		request.method = "GET";
		request.resource = path;
		request.write (output_data_stream);

		get_headers (input_data_stream);

		var content = new StringBuilder ();
		foreach (string line in new LineIterator(input_data_stream)) {
			content.append(line);
		}
		return content.str;
	}

	public string request_status_list (string host, uint16 port, string package_name, uint16 local_server_port) throws Error {
		print ("Status list: %s %d\n", host, port);
		var connection = get_connection (host, port);

		var output_data_stream = new DataOutputStream (connection.output_stream);
		var input_data_stream = new DataInputStream (connection.input_stream);

		var request = new Request ();
		request.method = "GET";
		request.resource = "/status/%s".printf (package_name);
		request.headers.set ("Local-Server", local_server_port.to_string ());
		request.write (output_data_stream);

		get_headers (input_data_stream);

		var content = new StringBuilder ();
		foreach (string line in new LineIterator(input_data_stream)) {
			content.append(line);
		}
		return content.str;
	}

	public string request_peer_list (string host, uint16 port, string package_name, uint16 local_server_port) throws Error {
		print ("Peer list: %s %d\n", host, port);
		var connection = get_connection (host, port);

		var output_data_stream = new DataOutputStream (connection.output_stream);
		var input_data_stream = new DataInputStream (connection.input_stream);

		var request = new Request ();
		request.method = "GET";
		request.resource = "/peers/%s".printf (package_name);
		request.headers.set ("Local-Server", local_server_port.to_string ());
		request.write (output_data_stream);

		get_headers (input_data_stream);

		var content = new StringBuilder ();
		foreach (string line in new LineIterator(input_data_stream)) {
			content.append(line);
		}
		return content.str;
	}
}
