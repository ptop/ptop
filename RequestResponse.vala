/* RequestResponse.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public class RequestResponse : Object {
	public bool valid { get; set; }
	public Gee.Map<string, string> headers { get; set; }
	public ServerInfo server_info { get; set; }

	construct {
		valid = false;
		headers = new Gee.HashMap<string, string>();
	}
}

public class Request : RequestResponse {
	public string method { get; set; }
	public string protocol { get; set; }
	public string resource { get; set; }
	public string body_content { get; set; }

	public Gee.List<string> resource_id { get; set; }

	construct {
		protocol = "HTTP1.1";
		body_content = "";
		resource_id = new Gee.ArrayList<string> ();
	}

	public void write (DataOutputStream data_output_stream) throws Error {
		data_output_stream.put_string ("%s %s %s\r\n".printf (method, resource, protocol));
		foreach (Gee.Map.Entry<string, string> header_entry in headers.entries) {
			data_output_stream.put_string ("%s: %s\r\n".printf (header_entry.key, header_entry.value));
		}
		data_output_stream.put_string ("\r\n");
		data_output_stream.put_string (body_content);
	}
}

errordomain HttpError {
	FILE_NOT_FOUND
}

public class Response : RequestResponse {
	public string protocol { get; set; }
	public int status_code { get; set; }
	public string reason { get; set; }

	public string content { get; set; }
	public bool streamable { get; set; }
	public string content_type { get; set; }
	public int64 content_length { get; set; }

	construct {
		streamable = false;
		content = "";
	}

	void read_response_line (DataInputStream data_stream) throws Error {
		string line = data_stream.read_line (null, null);
		string[] string_array = line.split (" ");
		protocol = string_array[0].strip ();
		status_code = string_array[1].strip ().to_int ();
		reason = string_array[2].strip ();
	}

	void read_header_values (DataInputStream data_stream) throws Error {
		string line = data_stream.read_line (null, null);
		string[] string_array;
		while (line.strip() != "") {
			string_array = line.split (":");
			headers[string_array[0].strip()] = string_array[1].strip();
			line = data_stream.read_line (null, null);
		}
	}

	public virtual bool read_header (DataInputStream data_stream) throws Error {
		read_response_line (data_stream);
		read_header_values (data_stream);
		return true;
	}

	public virtual bool write_header (DataOutputStream data_output_stream) throws Error {
		var length = content_length <= 0 ? content.size () : content_length;
		data_output_stream.put_string ("HTTP/1.1 200 OK\r\n", null);

		data_output_stream.put_string ("Server: %s\r\n".printf (server_info.server_string), null);
		data_output_stream.put_string ("Server-Instance: %s\r\n".printf (server_info.instance_id), null);

		data_output_stream.put_string ("Accept-Ranges: bytes\n", null);
		data_output_stream.put_string ("Content-Length: %s\r\n".printf (length.to_string()), null);
		data_output_stream.put_string ("Content-Type: %s\r\n".printf (content_type), null);
		data_output_stream.put_string ("Connection: close\r\n", null);
		data_output_stream.put_string ("\r\n", null);
		return true;
	}

	public virtual bool write (DataOutputStream output_stream) throws Error {
		output_stream.write_all (content, content.length, null, null);
		output_stream.flush (null);
		return true;
	}
}

public class FileNotFoundResponse : Response {
	public override bool write_header (DataOutputStream data_output_stream) throws Error {
		data_output_stream.put_string ("HTTP/1.1 404 File Not Found\r\n", null);
		data_output_stream.put_string ("Server: %s\r\n".printf (server_info.server_string), null);
		data_output_stream.put_string ("Server-Instance: %s\r\n".printf (server_info.instance_id), null);
		data_output_stream.put_string ("Connection: close\r\n\r\n", null);
		return true;
	}
	public override bool write (DataOutputStream output_stream) throws Error {
		return true;
	}
}

public class NotImplementedResponse : Response {
	public override bool write_header (DataOutputStream data_output_stream) throws Error {
		data_output_stream.put_string ("HTTP/1.1 501 Not implemented\r\n", null);
		data_output_stream.put_string ("Server: %s\r\n".printf (server_info.server_string), null);
		data_output_stream.put_string ("Server-Instance: %s\r\n".printf (server_info.instance_id), null);
		data_output_stream.put_string ("Connection: close\r\n\r\n", null);
		return true;
	}
	public override bool write (DataOutputStream output_stream) {
		return true;
	}
}

public class FileResponse : Response {
	public File file_to_stream { get; set; }
	public int64 chunk_number { get; set; }
	public int64 chunk_size { get; set; }

	construct {
		chunk_number = -1;
	}

	public FileResponse(File file_to_stream) {
		streamable = true;
		this.file_to_stream = file_to_stream;
	}

	public override bool write (DataOutputStream output_stream) throws Error {
		return chunk_number < 0 ? write_complete (output_stream) : write_chunk (output_stream);
	}

	private bool write_complete (DataOutputStream output_stream) throws Error {
		uchar[] buffer = new uchar[64*1024];
		size_t bytes_read = 0;
		size_t bytes_write = 0;
		bool result = false;
		var file_stream = file_to_stream.read (null);
		do {
			result = file_stream.read_all (buffer, 64*1024, out bytes_read, null);
			if (result) {
				print ("Sending.. %s", bytes_read.to_string ());
				result = output_stream.write_all (buffer, bytes_read, out bytes_write, null);
			}
		} while (result && bytes_read > 0 && bytes_write > 0);
		output_stream.flush(null);
		return result;
	}

	private bool write_chunk (DataOutputStream output_stream) throws Error {
		uchar[] buffer = new uchar[2*1024*1024];
		size_t bytes_read = 0;
		size_t bytes_write = 0;
		bool result = false;
		var file_stream = file_to_stream.read (null);
		file_stream.seek(2*1024*1024*chunk_number, SeekType.SET, null);
		result = file_stream.read_all (buffer, buffer.length, out bytes_read, null);
		if (result) {
			print ("Read from file: %s\n", bytes_read.to_string());
			result = output_stream.write_all (buffer, bytes_read, out bytes_write, null);
			print ("Sended: %s\n", bytes_write.to_string());
		}
		output_stream.flush(null);
		return result;
	}

	public override bool write_header (DataOutputStream data_output_stream) throws Error {
		var file_info = file_to_stream.query_info ("*", FileQueryInfoFlags.NONE, null);
		int64 size = chunk_number < 0 ? file_info.get_size () : chunk_size;
		data_output_stream.put_string ("HTTP/1.1 200 OK\r\n", null);
		data_output_stream.put_string ("Server: %s\r\n".printf (server_info.server_string), null);
		data_output_stream.put_string ("Server-Instance: %s\r\n".printf (server_info.instance_id), null);
		data_output_stream.put_string ("Accept-Ranges: bytes\r\n", null);
		data_output_stream.put_string ("Content-Length: %s\r\n".printf (size.to_string()), null);
		data_output_stream.put_string ("Connection: close\r\n", null);
		data_output_stream.put_string ("\r\n", null);
		return true;
	}
}
