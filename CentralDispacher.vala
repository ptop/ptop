/* CentralDispacher.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public class CentralDispatcher : Object {

	ApplicationData application_data { get; private set; }
	Client client { get; public set; }

	public signal void package_added (Package package);

	public CentralDispatcher (ApplicationData application_data) throws ThreadError {
		this.application_data = application_data;

		client = new Client ();
		initialize_existing_packages ();

		client.download_finnished.connect (on_download_finnished);
	}

	PackageList get_package_list () {
		return application_data.package_list;
	}

	void initialize_existing_packages () {
		var package_list = get_package_list ();
		package_list.new_package_added.connect (on_new_package_added);
		package_list.check_local_packages ();

		foreach (Package package in package_list.packages) {
			package.incomplete.connect (on_incomplete);
			package.check ();
		}
	}

	void on_new_package_added (Package package) {
		package.peer_list.peer_expired.connect (on_peer_expired);
		package.peer_list.peer_update.connect (on_peer_update);
		package.peer_list.peer_list_update.connect (on_peer_list_update);
		package.peer_list.check_peer_expire ();
		package.peer_list.force_update_peers ();
	}

	void on_peer_update (PeerList peer_list, Peer peer) {
		if (peer.port == application_data.configuration.port) {
			peer.is_local = true;
			return;
		}
		var package = peer_list.package;
		try {
			print ("Update file chunk list!\n");
			string file_chunk_status_list_json = client.request_status_list (peer.host, peer.port, package.package_name, application_data.configuration.port);
			peer.file_chunk_status_update (file_chunk_status_list_json);
		} catch (Error error) {
			print ("Removing peer.\n");
			peer_list.remove_peer (peer);
		}
	}

	void check_peer_local (Peer peer) {
		if (peer.port == application_data.configuration.port) {
			try {
				var resolver = Resolver.get_default ();
				var addresses = resolver.lookup_by_name ("kagami");
				foreach (var address in addresses) {
					if (address.to_string () == "127.0.0.1") {
						print ("Peer %s %s is local!\n", peer.host, peer.port.to_string ());
						peer.is_local = true;
					}
				}
			} catch (Error error) {
				print ("Error: %s", error.message);
			}
		}
	}

	void on_peer_expired (PeerList peer_list, Peer peer) {
		check_peer_local (peer);
		if (peer.is_local) {
			return;
		}
		var package = peer_list.package;
		print ("On %s Peer: %s %d has expired.\n", package.package_name, peer.host, peer.port);
		try {
			string peer_list_json = client.request_peer_list (peer.host, peer.port, package.package_name, application_data.configuration.port);
			peer_list.from_json (peer_list_json);
		} catch (Error error) {
			print ("Removing peer.\n");
			peer_list.remove_peer (peer);
		}
	}

	void on_download_finnished (DownloadInfo info) {
		var package = application_data.package_list.get_package_for_id (info.package_name);
		if (info.success) {
			package.check_and_complete_chunk (info.filename, info.chunk_number, info.checksum);
		} else {
			package.chunk_failed (info.filename, info.chunk_number);
		}
	}

	public void retrieve_package (string uri, string local_path) throws Error {
		var parsed_uri = new Soup.URI (uri);
		string host = parsed_uri.host;
		uint16 port = (uint16) parsed_uri.port;

		var package_files_json = client.request_package_metadata (uri);

		Package package = application_data.package_list.add_package (package_files_json, local_path);
		package.incomplete.connect (on_incomplete);
		package.save_package_list ();

		Peer peer = package.peer_list.add_new_peer (host, port);
		check_peer_local (peer);
		if (peer.is_local) {
			return;
		}

		var peer_list_json = client.request_peer_list (host, port, package.package_name, application_data.configuration.port);
		package.peer_list.from_json (peer_list_json);
		package_added (package);
		package.check ();
	}

	public void on_peer_list_update (PeerList peer_list, Peer peer) {
		if (peer.port == application_data.configuration.port) {
			peer.is_local = true;
			return;
		}
		var peer_list_json = client.request_peer_list (peer.host, peer.port, peer_list.package.package_name, application_data.configuration.port);
		peer_list.from_json (peer_list_json);
	}

	void on_incomplete (MissingChunkInformation chunk_info) {
		print ("Missing: %s %d\n", chunk_info.file_metadata.filename, chunk_info.chunk.consecutive_number);

		Peer? peer = chunk_info.package.peer_list.get_best_peer_for (chunk_info);

		if (peer == null) {
			return;
		}

		var download_info = new DownloadInfo () {
			host = peer.host,
			port = peer.port,
			local_path = chunk_info.package.local_path,
			package_name = chunk_info.package.package_name,
			filename = chunk_info.file_metadata.filename,
			chunk_number = chunk_info.chunk.consecutive_number,
			position_offset = chunk_info.chunk.position_offset ()
		};

		client.add_to_queue (download_info);
		client.process_queues ();
	}

	public void create_new_local_package (string path) {
		Package package = application_data.package_list.add_local_package (path);
		package_added (package);
	}
}
