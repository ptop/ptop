/* Peers.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public class FileChunkStatus : Object {
	public string filename { get; set; }
	public uint chunk_number { get; set; }
	public bool completed { get; set; }
}

public class Peer : Object {
	public string host { get; set; }
	public uint16 port { get; set; }
	public TimeVal last_seen_on { get; set; }
	public TimeVal last_update_on { get; set; }
	public int priority { get; set; }
	public Gee.List<FileChunkStatus> file_chunk_status_list { get; set; }
	public bool marked_for_removal { get; set; }
	public bool is_local { get; set; }

	public Peer (string host, uint16 port) {
		this.host = host;
		this.port = port;
		marked_for_removal = false;
		is_local = false;
		file_chunk_status_list = new Gee.ArrayList<FileChunkStatus> ();
	}

	public void update_last_seen_on (TimeVal input_last_seen_on) {
		if (input_last_seen_on.tv_sec > last_seen_on.tv_sec) {
			last_seen_on = input_last_seen_on;
		}
	}

	public void reset_last_seen_on () {
		last_seen_on = TimeVal();
		last_seen_on.get_current_time();
	}

	public void reset_last_update_on () {
		last_update_on = TimeVal();
		last_update_on.get_current_time();
	}

	public FileChunkStatus? get_existing_file_chunk_status (string filename, uint chunk_number) {
		foreach (FileChunkStatus status in file_chunk_status_list) {
			if (status.filename == filename && status.chunk_number == chunk_number) {
				return status;
			}
		}
		return null;
	}

	void update_old_file_chunk_status (FileChunkStatus old_status, FileChunkStatus new_status) {
		old_status.completed = new_status.completed;
	}

	void merge_file_chunk_status (FileChunkStatus new_file_chunk_status) {
		FileChunkStatus? existing_status = get_existing_file_chunk_status (new_file_chunk_status.filename, new_file_chunk_status.chunk_number);
		if (existing_status == null) {
			file_chunk_status_list.add (new_file_chunk_status);
		} else {
			update_old_file_chunk_status (existing_status, new_file_chunk_status);
		}
	}

	public void file_chunk_status_update (string file_chunk_status_list_json) {
		var json_parser = new Json.Parser ();
		json_parser.load_from_data (file_chunk_status_list_json, file_chunk_status_list_json.length);
		unowned Json.Node root_node = json_parser.get_root ();
		var status_array_json = root_node.get_array ();
		foreach (unowned Json.Node node in status_array_json.get_elements ()) {
			var file_chunk_status_json = node.get_object ();
			var file_chunk_status = new FileChunkStatus ();
			file_chunk_status.filename = file_chunk_status_json.get_string_member ("filename");
			file_chunk_status.chunk_number = (uint) file_chunk_status_json.get_int_member ("chunk_number");
			file_chunk_status.completed = file_chunk_status_json.get_boolean_member ("completed");
			merge_file_chunk_status (file_chunk_status);
		}
		reset_last_seen_on ();
		reset_last_update_on ();
	}
}

public class PeerList : Object {
	Gee.List<Peer> peers { get; private set; }
	public signal void peer_added (Peer peer);
	public signal void peer_expired (PeerList peer_list, Peer peer);
	public signal void peer_update (PeerList peer_list, Peer peer);
	public signal void peer_list_update (PeerList peer_list, Peer peer);

	const long PEER_EXPIRE_TIME_IN_SECONDS = 5 * 24 * 3600;
	const long PEER_UPDATE_TIME_IN_SECONDS = 10;

	public Package package { get; private set; }

	Mutex mutex;

	public PeerList (Package package) {
		this.package = package;
		peers = new Gee.LinkedList<Peer> ();
		mutex = new Mutex ();
		Timeout.add (10*1000, on_trigger_update_peer_list);
		Timeout.add (10*1000, on_check_peers);
	}

	public Gee.List<Peer> get_valid_peers () {
		var list = new Gee.ArrayList<Peer> ();
		foreach (Peer peer in peers) {
			if (!peer.marked_for_removal) {
				list.add (peer);
			}
		}
		return list;
	}

	void garbage_collect () {
		var list = new Gee.ArrayList<Peer> ();
		foreach (Peer peer in peers) {
			if (peer.marked_for_removal) {
				list.add (peer);
			}
		}
		foreach (Peer peer in list) {
			peers.remove (peer);
		}
		save_peer_list ();
	}

	bool on_trigger_update_peer_list () {
		foreach (Peer peer in get_valid_peers()) {
			peer_list_update (this, peer);
		}
		return true;
	}

	bool on_check_peers () {
		garbage_collect ();
		check_peer_expire ();
		check_peer_update ();
		return true;
	}

	public void force_update_peers () {
		foreach (Peer peer in get_valid_peers()) {
			peer_update (this, peer);
		}
	}

	public void check_peer_update () {
		var now = TimeVal();
		now.get_current_time();
		foreach (Peer peer in get_valid_peers()) {
			long delta_time = now.tv_sec - peer.last_seen_on.tv_sec;
			if (delta_time > PEER_UPDATE_TIME_IN_SECONDS) {
				peer_update (this, peer);
			}
		}
	}

	public void check_peer_expire () {
		var now = TimeVal();
		now.get_current_time();
		foreach (Peer peer in get_valid_peers()) {
			long delta_time = now.tv_sec - peer.last_seen_on.tv_sec;
			if (delta_time > PEER_EXPIRE_TIME_IN_SECONDS) {
				peer_expired (this, peer);
			}
		}
	}

	public void remove_peer (Peer peer) {
		peer.marked_for_removal = true;
	}

	public Peer? get_best_peer_for (MissingChunkInformation chunk_info) {
		Peer? candidate = null;
		uint chunk_number = chunk_info.chunk.consecutive_number;
		string filename = chunk_info.file_metadata.filename;
		foreach (Peer peer in peers) {
			if (peer.is_local) {
				continue;
			}
			FileChunkStatus? file_chunk_status = peer.get_existing_file_chunk_status (filename, chunk_number);
			if (file_chunk_status != null && file_chunk_status.completed) {
				if (candidate == null) {
					candidate = peer;
				} else if (peer.priority > candidate.priority) {
					candidate = peer;
				}
			}
		}

		return candidate;
	}

	public Peer add_new_peer (string hostname, uint16 port) {
		var now = TimeVal();
		now.get_current_time();
		return add_peer (hostname, port, now);
	}

	public Peer add_peer (string hostname, uint16 port, TimeVal last_seen_on) {
		Peer? peer = get_existing_peer (hostname, port);
		if (peer == null) {
			return create_new (hostname, port, last_seen_on);
		}
		update_peer (peer, last_seen_on);
		return peer;
	}

	void update_peer (Peer peer, TimeVal last_seen_on) {
		peer.update_last_seen_on (last_seen_on);
		save_peer_list ();
	}

	Peer create_new (string hostname, uint16 port, TimeVal last_seen_on) {
		var peer = new Peer (hostname, port);
		peer.last_seen_on = last_seen_on;
		peers.add (peer);
		peer_added (peer);
		save_peer_list ();
		return peer;
	}

	Peer? get_existing_peer (string hostname, uint16 port) {
		foreach (Peer each_peer in peers) {
			if (hostname == each_peer.host && port == each_peer.port) {
				return each_peer;
			}
		}
		return null;
	}

	public void from_json (string content) throws Error {
		var json_parser = new Json.Parser ();
		json_parser.load_from_data (content, content.length);
		unowned Json.Node root_node = json_parser.get_root ();
		var peers_array_json = root_node.get_array ();
		foreach (unowned Json.Node node in peers_array_json.get_elements ()) {
			var peer_json = node.get_object ();
			var host = peer_json.get_string_member ("host");
			var port = peer_json.get_int_member ("port");
			TimeVal last_seen_on = TimeVal ();
			last_seen_on.from_iso8601 (peer_json.get_string_member ("last_seen_on"));
			add_peer (host, (uint16) port, last_seen_on);
		}
		check_peer_expire ();
		check_peer_update ();
	}

	public string to_json () throws Error {
		var json_root = new Json.Node (Json.NodeType.ARRAY);
		var json_peers = new Json.Array ();
		json_root.set_array (json_peers);

		foreach (Peer peer in peers) {
			var json_peer = new Json.Object ();
			json_peer.set_string_member ("host", peer.host);
			json_peer.set_int_member ("port", peer.port);
			json_peer.set_string_member ("last_seen_on", peer.last_seen_on.to_iso8601 ());
			json_peers.add_object_element (json_peer);
		}

		var generator = new Json.Generator();
		generator.set_root (json_root);
		return generator.to_data (null);
	}

	public void save_peer_list () throws Error {
		mutex.lock ();
		var file = File.new_for_path (get_peer_list_path ());
		OutputStream file_stream;
		if (file.query_exists ()) {
			file_stream = file.replace (null, false, FileCreateFlags.REPLACE_DESTINATION);
		} else {
			file_stream = file.create (FileCreateFlags.REPLACE_DESTINATION);
		}
		var file_data_stream = new DataOutputStream (file_stream);
		file_data_stream.put_string (to_json ());
		file_data_stream.close ();
		file_stream.close ();
		mutex.unlock ();
	}

	public string get_peer_list_path () {
		return package.get_package_path () + "/peers.list";
	}
}
