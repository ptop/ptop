/* Executors.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public abstract class AbstractExecutor : Object {
	protected ApplicationData application_data { get; private set; }

	public AbstractExecutor (ApplicationData application_data) {
		this.application_data = application_data;
	}
}


public class StatusExecutor : AbstractExecutor {
	public StatusExecutor (ApplicationData application_data) {
		base (application_data);
	}

	public Response execute (Request request) {
		print("Enter GetStatusExecutor\n");

		string package_id = request.resource_id[1];
		print ("Package Id: %s\n", package_id);
		var package = application_data.package_list.get_package_for_id (package_id);

		var response = new Response () { server_info = application_data.server_info };
		response.content_type = "application/json";
		response.content = package.file_chunk_status_list_as_json ();
		return response;
	}
}

public class PeersExecutor : AbstractExecutor {
	public PeersExecutor (ApplicationData application_data) {
		base (application_data);
	}

	public Response execute (Request request) {
		print ("Enter GetPeersExecutor\n");
		string package_id = request.resource_id[1];
		print ("Package Id: %s\n", package_id);
		var package = application_data.package_list.get_package_for_id (package_id);

		//package.peer_list.add_new_peer ();

		var response = new Response () { server_info = application_data.server_info };
		response.content_type = "application/json";
		response.content = package.peer_list.to_json ();
		return response;
	}
}

public class ListExecutor : AbstractExecutor {
	public ListExecutor (ApplicationData application_data) {
		base (application_data);
	}

	public Response execute (Request request) {
		print ("Request - GET LIST\n");
		string package_id = request.resource_id[1];
		print ("Package Id: %s\n", package_id);
		var package = application_data.package_list.get_package_for_id (package_id);
		var response = new Response () { server_info = application_data.server_info };
		response.content_type = "application/json";
		response.content += package.to_json ();

		return response;
	}
}

public class FileExecutor : AbstractExecutor {
	public FileExecutor (ApplicationData application_data) {
		base (application_data);
	}

	public Response execute (Request request) {
		print ("Request - GET FILE\n");
		string package_id = request.resource_id[1];

		var package = application_data.package_list.get_package_for_id (package_id);

		string path = "";
		foreach (string id in request.resource_id.slice (2, request.resource_id.size)) {
			path += id;
		}

		if (path != "") {
			string local_path = package.get_local_file_path (path);
			var file_to_stream = File.new_for_path (package.get_local_file_path (path));
			if (!file_to_stream.query_exists (null)) {
				print ("File '%s' doesn't exist.\n", file_to_stream.get_path ());
				return new FileNotFoundResponse () { server_info = application_data.server_info };
			}
			var response = new FileResponse (file_to_stream) { server_info = application_data.server_info };
			if ("Chunk" in request.headers) {
				response.chunk_number = request.headers["Chunk"].to_int64();

				foreach (FileMetadata file_metadata in package.files) {
					if (file_metadata.filename == path) {
						response.chunk_size = file_metadata.chunks[(int)response.chunk_number].size;
					}
				}
			}
			return response;
		} else {
			print ("File not in header\n");
		}
		return new Response () { server_info = application_data.server_info };
	}
}
