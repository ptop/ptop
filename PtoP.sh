valac -o PtoP \
	--pkg gee-1.0 \
	--pkg gio-2.0 \
	--pkg json-glib-1.0 \
	--pkg libsoup-2.4 \
	--pkg gtk+-2.0 \
	PackageMetaData.vala \
	Tools.vala \
	Client.vala \
	Server.vala \
	Executors.vala \
	RequestResponse.vala \
	ClientGui.vala \
	Peers.vala \
	PtoP.vala \
	CentralDispacher.vala
