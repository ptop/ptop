/* PackageMetaData.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public class MissingChunkInformation : Object {
	public Package package { get; private set; }
	public FileMetadata file_metadata { get; private set; }
	public Chunk chunk { get; private set; }

	public MissingChunkInformation (Package package, FileMetadata file_metadata, Chunk chunk) {
		this.package = package;
		this.file_metadata = file_metadata;
		this.chunk = chunk;
	}
}

public class Chunk : Object {
	public static int SIZE = 2*1024*1024;
	public int consecutive_number { get; set; }
	public int64 size { get; set; }
	public string checksum { get; set; }
	public bool complete { get; set; }

	public int64 position_offset () {
		return SIZE * consecutive_number;
	}

	public bool check_checksum_and_complete (string calculated_checksum) {
		if (this.checksum == calculated_checksum) {
			complete = true;
		}
		return complete;
	}

	public void mark_complete () {
		complete = true;
	}
}

public class FileMetadata : Object {
	public int64 file_size { get; set; }
	public string filename {get; set; }
	public Gee.List<Chunk> chunks = new Gee.ArrayList<Chunk> ();

	public int get_chunks_complete () {
		int counter = 0;
		foreach (Chunk chunk in chunks) {
			if (chunk.complete) {
				counter++;
			}
		}
		return counter;
	}
}

public class Package : Object {
	public string local_path { get; private set; }
	public string package_name { get; private set; }
	public Gee.List<FileMetadata> files {get; private set; }
	public PeerList peer_list { get; private set; }

	public signal void incomplete (MissingChunkInformation missing_chunk_information);

	public Package (string package_name, string local_path) throws Error {
		files = new Gee.ArrayList<FileMetadata> ();
		this.package_name = package_name;
		this.local_path = local_path;
		peer_list = new PeerList (this);
	}

	public Package.from_json (string json_string, string local_path) throws Error {
		files = new Gee.ArrayList<FileMetadata> ();
		this.local_path = local_path;
		create_from_json (json_string);
		peer_list = new PeerList (this);

		add_package_directory ();
	}

	public string get_package_path () {
		return local_path + "/" + package_name;
	}

	public string get_local_file_path (string sub_path) {
		return get_package_path () + "/" + sub_path;
	}

	public string get_package_list_path () {
		return get_package_path () + "/package.list";
	}

	public string get_file_full_path (FileMetadata file_metadata) {
		return get_package_path () + "/" + file_metadata.filename;
	}

	public FileMetadata? get_file_metadata (string filename) {
		foreach (FileMetadata file in files) {
			if (file.filename == filename) {
				return file;
			}
		}
		return null;
	}

	public Chunk? get_chunk_for (string filename, int chunk_number) {
		foreach (FileMetadata file in files) {
			if (file.filename == filename) {
				return file.chunks[(int)chunk_number];
			}
		}
		return null;
	}

	public void save_package_list () throws Error {
		add_package_directory ();

		var file = File.new_for_path (get_package_list_path ());
		var file_stream = file.create (FileCreateFlags.REPLACE_DESTINATION);
		var file_data_stream = new DataOutputStream (file_stream);
		file_data_stream.put_string (to_json ());
		file_data_stream.close ();
		file_stream.close ();
	}

	void add_package_directory () throws Error {
		var file = File.new_for_path (get_package_path ());
		if (!file.query_exists ()) {
			file.make_directory ();
		}
	}

	public void create_from_json (string content) throws Error {
		var parser = new Json.Parser ();
		parser.load_from_data (content, content.length);

		unowned Json.Node root_node = parser.get_root ();
		var package_json = root_node.get_object ();
		package_name = package_json.get_string_member ("package_name");
		var array = package_json.get_array_member ("files");
		foreach (unowned Json.Node node in array.get_elements ()) {
			var file_object = node.get_object ();
			var file_metadata = new FileMetadata ();
			file_metadata.filename = file_object.get_string_member ("file_name");
			file_metadata.file_size = file_object.get_int_member ("file_size");
			files.add (file_metadata);
			foreach (unowned Json.Node sub_node in file_object.get_array_member ("chunks").get_elements ()) {
				var chunk_object = sub_node.get_object ();
				var chunk = new Chunk ();
				file_metadata.chunks.add (chunk);
				chunk.consecutive_number = (int) chunk_object.get_int_member ("consecutive_number");
				chunk.size = chunk_object.get_int_member ("size");
				chunk.checksum = chunk_object.get_string_member ("checksum");
			}
		}
	}

	public string file_chunk_status_list_as_json () throws Error {
		var json_root = new Json.Node (Json.NodeType.ARRAY);
		var json_status_list = new Json.Array ();
		json_root.set_array (json_status_list);

		foreach (FileMetadata file in files) {
			foreach (Chunk chunk in file.chunks) {
				var json_status = new Json.Object ();
				json_status.set_string_member ("filename", file.filename);
				json_status.set_int_member ("chunk_number", chunk.consecutive_number);
				json_status.set_boolean_member ("completed", chunk.complete);
				json_status_list.add_object_element (json_status);
			}
		}

		var generator = new Json.Generator();
		generator.set_root (json_root);
		return generator.to_data (null);
	}

	public string to_json () {
		var json_root = new Json.Node (Json.NodeType.OBJECT);
		var json_package = new Json.Object ();
		json_package.set_string_member ("package_name", package_name);
		var json_files = new Json.Array ();
		foreach (FileMetadata file in files) {
			var json_file = new Json.Object ();
			json_file.set_string_member ("file_name", file.filename);
			json_file.set_int_member ("file_size", file.file_size);
			var json_chunks = new Json.Array ();
			foreach (Chunk chunk in file.chunks) {
				var json_chunk = new Json.Object ();
				json_chunk.set_int_member ("consecutive_number", chunk.consecutive_number);
				json_chunk.set_int_member ("size", chunk.size);
				json_chunk.set_string_member ("checksum", chunk.checksum);
				json_chunks.add_object_element (json_chunk);
			}
			json_file.set_array_member ("chunks", json_chunks);
			json_files.add_object_element (json_file);
		}
		json_package.set_array_member ("files", json_files);
		json_root.set_object (json_package);
		var generator = new Json.Generator();
		generator.set_root (json_root);
		return generator.to_data (null);
	}

	void check_chunk_checksum (File file, Chunk chunk) {
		try {
			var file_stream = file.read (null);
			var data_stream = new DataInputStream (file_stream);
			uchar[] buffer = new uchar[chunk.SIZE];
			size_t bytes_read = 0;
			file_stream.seek (chunk.position_offset (), SeekType.SET, null);
			bool result = data_stream.read_all (buffer, buffer.length, out bytes_read, null);
			if (result) {
				var checksum = new Checksum (ChecksumType.SHA1);
				checksum.update (buffer, bytes_read);
				chunk.complete = checksum.get_string () == chunk.checksum;
			}
		} catch (Error error) {
			print (error.message+"\n");
		}
	}

	public void check_and_complete_chunk (string filename, int chunk_number, string checksum) {
		var file_metadata = get_file_metadata (filename);
		if (file_metadata == null) {
			return;
		}
		var chunk = file_metadata.chunks[chunk_number];
		if (!chunk.check_checksum_and_complete (checksum)) {
			incomplete (new MissingChunkInformation (this, file_metadata, chunk));
		}
	}

	public void chunk_failed (string filename, int chunk_number) {
		var file_metadata = get_file_metadata (filename);
		if (file_metadata == null) {
			return;
		}
		var chunk = file_metadata.chunks[chunk_number];
		incomplete (new MissingChunkInformation (this, file_metadata, chunk));
	}

	public void check () {
		foreach (FileMetadata file_metadata in files) {
			File file = File.new_for_path (get_file_full_path (file_metadata));
			if (!file.query_exists ()) {
				foreach (Chunk chunk in file_metadata.chunks) {
					chunk.complete = false;
					incomplete (new MissingChunkInformation (this, file_metadata, chunk));
				}
			} else {
				foreach (Chunk chunk in file_metadata.chunks) {
					check_chunk_checksum (file, chunk);
					if (!chunk.complete) {
						incomplete (new MissingChunkInformation (this, file_metadata, chunk));
					}
				}
			}
		}
	}

	int read_chunk (File file, FileMetadata file_metadata) {
		try {
			Chunk chunk;
			uchar[] buffer = new uchar[chunk.SIZE];
			int chunk_number = 0;
			size_t bytes_read = 0;

			var file_stream = file.read (null);
			var data_stream = new DataInputStream (file_stream);

			do {
				bool result = data_stream.read_all (buffer, buffer.length, out bytes_read, null);
				if (result && bytes_read > 0) {
					var checksum = new Checksum (ChecksumType.SHA1);
					checksum.update (buffer, bytes_read);
					chunk = new Chunk () {
						consecutive_number = chunk_number,
						size = bytes_read,
						checksum = checksum.get_string ()
					};
					chunk_number++;
					chunk.mark_complete ();
					file_metadata.chunks.add (chunk);
				}
			} while (bytes_read > 0);
		} catch (Error e) {
			print ("Error: %s\n", e.message);
			return 1;
		}
		return 0;
	}

	public void grap_data_from_local_files () {
		try {
			var directory = File.new_for_path (get_package_path ());
			var enumerator = directory.enumerate_children ("*", 0, null);

			foreach (FileInfo file_info in new FileIterator(enumerator)) {
				if (file_info.get_file_type () == FileType.REGULAR) {
					var file_metadata = new FileMetadata();
					file_metadata.filename = file_info.get_name ();
					file_metadata.file_size = file_info.get_size ();
					files.add (file_metadata);

					read_chunk (File.new_for_path (directory.get_path ()+"/"+file_info.get_name ()), file_metadata);
				}
			}
		} catch (Error error) {
			print ("Error: %s\n", error.message);
		}
	}
}

public class LocalPackageInfo : Object {
	public string local_path { get; set; }
	public string name { get; set; }

	public string get_package_path () {
		return local_path + "/" +name;
	}
}


public class PackageList : Object {
	public Gee.List<Package> packages { get; private set; }

	construct {
		packages = new Gee.ArrayList<Package>();
	}

	public signal void new_package_added (Package package);

	public Package? get_package_for_id (string package_id) {
		foreach (Package package in packages) {
			if (package.package_name == package_id) {
				return package;
			}
		}
		return null;
	}

	void add_new_package (Package package) {
		new_package_added (package);
		packages.add (package);
	}

	Gee.List<LocalPackageInfo> load_local_package_infos (string json_content) {
		var package_info_list = new Gee.ArrayList<LocalPackageInfo> ();
		var parser = new Json.Parser ();
		parser.load_from_data (json_content, json_content.length);

		unowned Json.Node root_node = parser.get_root ();
		var package_list_root_json = root_node.get_object ();
		var array = package_list_root_json.get_array_member ("packages");
		foreach (unowned Json.Node node in array.get_elements ()) {
			var package_json = node.get_object ();
			var package_info = new LocalPackageInfo ();
			package_info.local_path = package_json.get_string_member ("local_path");
			package_info.name = package_json.get_string_member ("name");
			package_info_list.add (package_info);
		}
		return package_info_list;
	}

	public void check_local_packages () throws Error {
		print ("Check local packages\n");
		var path = "available_packages.json";
		var avalable_packages_file = File.new_for_path (path);
		if (!avalable_packages_file.query_exists ()) {
			print ("File 'available_packages.json' not found\n");
			return;
		}

		string packages_json;
		avalable_packages_file.load_contents (null, out packages_json);
		var local_packages_list = load_local_package_infos (packages_json);

		foreach (LocalPackageInfo info in local_packages_list) {
			string candidate = info.get_package_path ();
			string package_file_path = candidate + "/package.list";
			string peers_file_path = candidate + "/peers.list";
			var package_file = File.new_for_path (package_file_path);
			var peers_file = File.new_for_path (peers_file_path);
			if (package_file.query_exists () && peers_file.query_exists ()) {
				string package_json_string;
				string peers_json_string;
				package_file.load_contents (null, out package_json_string);
				peers_file.load_contents (null, out peers_json_string);
				var package = new Package.from_json (package_json_string, info.local_path);
				package.peer_list.from_json (peers_json_string);
				add_new_package (package);
			}
		}
	}

	public void check_all () {
		foreach (Package package in packages) {
			package.check ();
		}
	}

	public Package add_package (string package_json_content, string local_path) throws Error {
		var package = new Package.from_json (package_json_content, local_path);
		packages.add (package);
		save_packages ();
		return package;
	}

	void save_packages () throws Error  {
		string package_file_path = "available_packages.json";
		var file = File.new_for_path (package_file_path);
		var file_stream = file.create (FileCreateFlags.REPLACE_DESTINATION);
		var file_data_stream = new DataOutputStream (file_stream);
		file_data_stream.put_string (to_json ());
		file_data_stream.close ();
		file_stream.close ();
	}

	string to_json () {
		var json_root = new Json.Node (Json.NodeType.OBJECT);
		var json_package_list = new Json.Object ();
		var json_packages = new Json.Array ();

		foreach (Package package in packages) {
			var json_package = new Json.Object ();
			json_package.set_string_member ("name", package.package_name);
			json_package.set_string_member ("local_path", package.local_path);
			json_packages.add_object_element (json_package);
		}
		json_package_list.set_array_member ("packages", json_packages);
		json_root.set_object (json_package_list);
		var generator = new Json.Generator();
		generator.set_root (json_root);
		return generator.to_data (null);
	}

	public Package add_local_package (string path) throws Error {
		string package_name = Path.get_basename (path);
		string local_path = Path.get_dirname (path);
		var package = new Package (package_name, local_path);
		package.grap_data_from_local_files ();
		packages.add (package);
		package.save_package_list ();
		package.peer_list.save_peer_list ();
		save_packages ();
		return package;
	}

}
