/* Tools.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public interface SimpleIterator<G> : Object {
	public SimpleIterator<G> iterator () {
		return this;
	}
	public abstract G @get ();
	public abstract bool next ();
}

public interface NullIterator<G> : Object {
	public NullIterator<G> iterator () {
		return this;
	}
	public abstract G next_value ();
}

public class GenericIterator : Object, NullIterator<string?> {
	private EachIteration iteration_delegate;
	public delegate string? EachIteration ();

	public GenericIterator (EachIteration iteration_delegate) {
		this.iteration_delegate = iteration_delegate;
	}

	public string? next_value () {
		return iteration_delegate ();
	}
}

public class LineIterator : Object, NullIterator<string?> {
	public DataInputStream input_stream { get; set; }

	public LineIterator (DataInputStream input_stream) {
		this.input_stream = input_stream;
	}

	public string? next_value () {
		try {
			return input_stream.read_line (null, null);
		} catch (Error e) {
			print ("Error: %s\n", e.message);
		}
		return null;
	}
}

public class FileIterator : Object, NullIterator<FileInfo?> {
	public FileEnumerator enumerator { get; set; }

	public FileIterator (FileEnumerator enumerator) {
		this.enumerator = enumerator;
	}

	public FileInfo? next_value () {
		try {
			return enumerator.next_file (null);
		} catch (Error e) {
			print ("Error: %s\n", e.message);
		}
		return null;
	}
}


public class ThrottledInputStream : FilterInputStream {
	long rate_limit { get; private set; }

	public ThrottledInputStream (InputStream base_stream, long rate_limit) {
		Object (base_stream: base_stream);
		this.rate_limit = rate_limit;
	}

	public override ssize_t read_fn (void* buffer, size_t count, GLib.Cancellable? cancellable = null) throws GLib.Error {
		ssize_t size = base_stream.read (buffer, count, cancellable);
		long sleep_time = (size * 1000000 / rate_limit);
		if (sleep_time > 0) {
			Thread.usleep(sleep_time);
		}
		return size;
	}
}


public class ThrottledOutputStream : FilterOutputStream {
	long rate_limit { get; private set; }

	public ThrottledOutputStream (GLib.OutputStream base_stream, long rate_limit) {
		Object ( base_stream: base_stream );
		this.rate_limit = rate_limit;
	}

	public override ssize_t write_fn (void* buffer, size_t count, GLib.Cancellable? cancellable = null) throws GLib.Error {
		ssize_t size = base_stream.write (buffer, count, cancellable);
		long sleep_time = (size * 1000000 / rate_limit);
		if (sleep_time > 0) {
			Thread.usleep(sleep_time);
		}
		return size;
	}
}

public class RingBuffer<G> : Object, Gee.Iterable<G>, Gee.Iterator<G> {
	public int ring_size { get; private set; }
	public int size { get; private set; }

	public G[] ring;

	int first_index = 0;
	int last_index = 0;

	public RingBuffer (int ring_size) {
		this.ring_size = ring_size;
		ring = new G[ring_size];
		size = 0;
	}

	public void add (G element) {
		if (last_index == first_index && size != 0) {
			first_index++;
			if (first_index >= ring_size) {
				first_index = 0;
			}
		} else {
			size++;
		}
		ring[last_index] = element;
		last_index++;
		if (last_index >= ring_size) {
			last_index = 0;
		}
	}

	Type element_type {
		get {
			return typeof(G);
		}
	}

	int current_index;

	public Gee.Iterator<G> iterator () {
		first();
		return this;
	}

	public bool first () {
		current_index = first_index;
		return true;
	}

	public bool has_next () {
		int next_index = current_index + 1;
		if (next_index >= ring_size) {
			next_index = 0;
		}
		return next_index != last_index;
	}

	public bool next() {
		if (size == 0) {
			return false;
		}

		current_index++;
		if (current_index >= ring_size) {
			current_index = 0;
		}
		return current_index != last_index;
	}

	public new G get () {
		return ring[current_index];
	}

	public void remove() {
	}
}
