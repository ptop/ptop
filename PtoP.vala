/* PtoP.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

public class Configuration : Object {
	public int64 max_upload_speed { get; set; }
	public string packages_directory { get; set; }
	public uint16 port { get; set; }

	construct {
		set_default_values ();
	}

	void set_default_values () {
		max_upload_speed = -1;
		packages_directory = Environment.get_current_dir ();
	}
}

public class ServerInfo : Object {
	public string server_string { get; set; }
	public string instance_id { get; set; }
}

public class ApplicationData : Object {
	public PackageList package_list { get; private set; }
	public Configuration configuration { get; private set; }
	public ServerInfo server_info { get; private set; }

	construct {
		server_info = new ServerInfo ();
		generate_serveri_instance_id ();

		configuration = new Configuration ();
		package_list = new PackageList ();
	}

	void generate_serveri_instance_id () {
		string id = "";
		id += Random.next_int ().to_string ();
		TimeVal time = TimeVal ();
		time.get_current_time ();
		id += time.to_iso8601 ();

		server_info.server_string = "PtoP Server";
		server_info.instance_id = Checksum.compute_for_string (ChecksumType.SHA256, id);
	}
}

int main (string[] args) {

	int port = 6767;

	var entry = OptionEntry() {
		long_name = "port",
		short_name = 'p',
		arg = OptionArg.INT,
		arg_data = &port,
		description = "Server Port",
		arg_description = "P"
	};

	var context = new OptionContext ("Yes...");

	context.add_main_entries({entry}, null);

	try {
		context.parse (ref args);
	} catch (OptionError error) {
		print (error.message);
	}

	if (!Thread.supported ()) {
		error ("Cannot run without thread support.");
	}

	Gtk.init (ref args);

	ApplicationData application_data = new ApplicationData ();
	application_data.configuration.port = (uint16) port;

	try {
		Server server = new Server (application_data, port);
		CentralDispatcher central_dispatcher = new CentralDispatcher (application_data);

		ApplicationGui application_gui = new ApplicationGui (server, central_dispatcher, application_data);

	} catch (Error error) {
		print ("%s\n", error.message);
	}

	Gtk.main ();

	return 0;
}

