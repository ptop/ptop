/* ClientGui.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */

using Gtk;

public class PeerDialog : Dialog {
	PeerList peer_list;

	public PeerDialog (PeerList peer_list) {
		this.peer_list = peer_list;
		title = "List of Peers";
		has_separator = false;
		border_width = 5;
		set_default_size (350, 100);

		add_button (STOCK_CLOSE, ResponseType.CLOSE);

		var tree = new TreeView ();
		setup_treeview (tree);

		vbox.spacing = 10;

		var scroll_window = new Gtk.ScrolledWindow (null, null);
		scroll_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC);
		scroll_window.add (tree);

		vbox.pack_start (scroll_window, true, true, 0);
		this.response.connect (on_response);
		show_all ();
	}

	void on_response (Dialog source, int response_id) {
		switch (response_id) {
			case ResponseType.CLOSE:
				destroy ();
				break;
		}
	}

	 private void setup_treeview (TreeView tree) {
		var model = new TreeStore (7, typeof (string), typeof (string), typeof (string), typeof (string), typeof (string), typeof (string), typeof (string));
		tree.set_model (model);

		tree.insert_column_with_attributes (-1, "Host", new CellRendererText (), "text", 0);
		tree.insert_column_with_attributes (-1, "Port", new CellRendererText (), "text", 1);
		tree.insert_column_with_attributes (-1, "Last seen on", new CellRendererText (), "text", 2);
		tree.insert_column_with_attributes (-1, "Local", new CellRendererText (), "text", 3);
		tree.insert_column_with_attributes (-1, "Filename", new CellRendererText (), "text", 4);
		tree.insert_column_with_attributes (-1, "Chunk number", new CellRendererText (), "text", 5);
		tree.insert_column_with_attributes (-1, "Completed", new CellRendererText (), "text", 6);

		TreeIter root;
		TreeIter entry;

		foreach (Peer peer in peer_list.get_valid_peers ()) {
			model.append (out root, null);
			model.set_value (root, 0, peer.host);
			model.set_value (root, 1, peer.port.to_string ());
			model.set_value (root, 2, peer.last_seen_on.to_iso8601 ());
			model.set_value (root, 3, peer.is_local.to_string ());
			foreach (FileChunkStatus status in peer.file_chunk_status_list) {
				model.append (out entry, root);
				model.set_value (entry, 4, status.filename);
				model.set_value (entry, 5, status.chunk_number.to_string ());
				model.set_value (entry, 6, status.completed.to_string ());
			}
		}
	}
}


public class PackageUrlDialog : Dialog {
	Entry package_url_entry;
	FileChooserButton chooser;

	public string package_url { get; private set; }
	public string package_local_path { get; private set; }

	construct {
		this.has_separator = false;
		this.border_width = 1;
		this.modal = true;
		response.connect (on_response);
		create_widgets ();
		package_url_entry.set_text ("http://localhost:6767/list/TestData");
		show_all ();
	}

	HBox create_entry_pair (out Entry entry, string label_text, int entry_characters_legth = 10) {
		HBox entry_pair_box = new HBox (false, 0);
		entry = new Entry ();
		entry.set_width_chars (entry_characters_legth);

		Label label = new Label (label_text);
		label.set_width_chars (10);
		label.set_alignment (0.0f, 0.5f);

		entry_pair_box.pack_start (label, false, false, 0);
		entry_pair_box.pack_start (entry, true, true, 0);

		return entry_pair_box;
	}


	void create_widgets () {
		add_button (STOCK_OK, ResponseType.OK);
		add_button (STOCK_CANCEL, ResponseType.CANCEL);

		var package_url_widget_pair = create_entry_pair (out package_url_entry, "URL:", 30);
		this.vbox.pack_start (package_url_widget_pair, true, true, 0);

		chooser = new FileChooserButton ("Choose..", FileChooserAction.SELECT_FOLDER);
		this.vbox.pack_start (chooser, true, true, 0);
	}

	private void on_response (Dialog source, int response_id) {
		switch (response_id) {
			case ResponseType.OK:
				package_url = package_url_entry.get_text ();
				package_local_path = chooser.get_filename ();
				hide ();
				break;
			case ResponseType.CANCEL:
				hide ();
				break;
		}
	}
}

public class ApplicationGui : Window {
	CentralDispatcher central_dispatcher;
	Server server;
	TreeView tree_view;
	ListStore model;

	ApplicationData application_data { get; private set; }

	public ApplicationGui (Server server, CentralDispatcher central_dispatcher, ApplicationData application_data) {
		this.central_dispatcher = central_dispatcher;
		this.server = server;
		this.application_data = application_data;

		central_dispatcher.package_added.connect (on_package_added);

		title = "PtoP Client Port: " + application_data.configuration.port.to_string ();
		set_default_size (500, 500);
		destroy.connect (Gtk.main_quit);

		setup_view ();

		realize.connect (on_realize);

		Timeout.add (5000, on_timer);

		show_all ();
	}

	void setup_view () {
		var main_box = new Gtk.VBox(false, 0);
		add (main_box);

		tree_view = new TreeView ();
		main_box.pack_start (tree_view, true, true, 0);

		model = new ListStore (5, typeof (string), typeof (string), typeof (string), typeof (string), typeof (string));

		tree_view.insert_column_with_attributes (-1, "Package", new CellRendererText (), "text", 0);
		tree_view.insert_column_with_attributes (-1, "Filename", new CellRendererText (), "text", 1);
		tree_view.insert_column_with_attributes (-1, "Size", new CellRendererText (), "text", 2);
		tree_view.insert_column_with_attributes (-1, "Chunks", new CellRendererText (), "text", 3);
		tree_view.insert_column_with_attributes (-1, "Complete", new CellRendererText (), "text", 4);

		tree_view.set_model (model);

		tree_view.button_release_event.connect (on_tree_button_released);

		var button_box = new Gtk.HBox(true, 0);
		Button button;

		button = new Gtk.Button.with_label ("Refresh");
		button.clicked.connect (on_refresh_clicked);
		button_box.pack_start (button, false, false, 0);

		button = new Gtk.Button.with_label ("Download package");
		button.clicked.connect (on_download_package_clicked);
		button_box.pack_start (button, false, false, 0);

		button = new Gtk.Button.with_label ("Add new package");
		button.clicked.connect (on_add_new_package_clicked);
		button_box.pack_start (button, false, false, 0);

		main_box.pack_start (button_box, false, false, 0);
	}

	bool on_timer() {
		refresh_model ();
		return true;
	}

	void on_realize () {
		initialize_model ();
	}

	void on_refresh_clicked () {
		refresh_model ();
	}

	void on_download_package_clicked () {
		var dialog = new PackageUrlDialog ();
		if (dialog.run () == (int) ResponseType.OK) {
			try {
				central_dispatcher.retrieve_package (dialog.package_url, dialog.package_local_path);
			}	catch (Error error) {
				print ("%s\n", error.message);
			}
		}
	}

	void on_add_new_package_clicked () {
		var file_chooser = new FileChooserDialog ("Choose folder...", null, FileChooserAction.SELECT_FOLDER, Gtk.STOCK_CANCEL, ResponseType.CANCEL, Gtk.STOCK_OK, ResponseType.ACCEPT, null);
		if (file_chooser.run() == Gtk.ResponseType.ACCEPT) {
			string file_path;
			file_path = file_chooser.get_filename ();
			file_chooser.destroy ();
			central_dispatcher.create_new_local_package (file_path);
		} else {
			file_chooser.destroy ();
		}
	}


	void initialize_model () {
		model.clear ();
		TreeIter iter;
		foreach (Package package in application_data.package_list.packages) {
			add_package_to_model (package);
		}
	}

	void on_package_added (Package package) {
		add_package_to_model (package);
	}

	void add_package_to_model (Package package) {
		TreeIter iter;
		foreach (FileMetadata file in package.files) {
			model.append (out iter);
			model.set_value (iter, 0, package.package_name);
			model.set_value (iter, 1, file.filename);
			model.set_value (iter, 2, file.file_size.to_string ());
			model.set_value (iter, 3, "%d/%d".printf (file.chunks.size, file.get_chunks_complete ()));
			model.set_value (iter, 4, "%.2f%%".printf (file.get_chunks_complete ()*100.0/file.chunks.size));
		}
	}

	bool update_model_callback (TreeModel current_model, TreePath path, TreeIter iter) {

		Value package_name_value;
		Value filename_value;

		model.get_value (iter, 0, out package_name_value);
		model.get_value (iter, 1, out filename_value);

		Package? package = application_data.package_list.get_package_for_id (package_name_value.get_string ());
		if (package == null) {
			return false;
		}

		FileMetadata? file = package.get_file_metadata (filename_value.get_string ());
		if (file == null) {
			return false;
		}

		model.set_value (iter, 0, package.package_name);
		model.set_value (iter, 1, file.filename);
		model.set_value (iter, 2, file.file_size.to_string ());
		model.set_value (iter, 3, "%d/%d".printf (file.chunks.size, file.get_chunks_complete ()));
		model.set_value (iter, 4, "%.2f%%".printf (file.get_chunks_complete ()*100.0/file.chunks.size));

		return false;
	}

	void refresh_model () {
		model.foreach (update_model_callback);
	}

	bool on_tree_button_released (Gdk.EventButton event) {
		if (event.button == 3) {
			var menu = new Gtk.Menu ();
			var menu_item = new Gtk.MenuItem.with_label ("Show peers");
			menu_item.activate.connect (on_show_peers_activated);
			menu.add (menu_item);
			menu.show_all ();
			menu.popup (null, null, null, event.button, event.time);
		}
		return true;
	}

	void on_show_peers_activated () {
		var selection = tree_view.get_selection ();
		if (selection != null) {
			Gtk.TreeIter iter;
			unowned Gtk.TreeModel model;
			selection.get_selected  (out model, out iter);
			Value package_name_value;
			model.get_value (iter, 0, out package_name_value);
			Package? package = application_data.package_list.get_package_for_id (package_name_value.get_string ());
			if (package != null) {
				var peer_dialog = new PeerDialog (package.peer_list);
				peer_dialog.run ();
			}
		}
	}

}
