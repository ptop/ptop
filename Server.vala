/* Server.vala
 *
 * Copyright (C) 2010  Tomaž Vajngerl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Tomaž Vajngerl <quikee@gmail.com>
 */
public class Server : Object {
	ThreadedSocketService service = new ThreadedSocketService (10);

	ApplicationData application_data { get; private set; }

	public Server (ApplicationData application_data, int port) throws Error {
		this.application_data = application_data;
		service.add_inet_port ((uint16)port, null);
		service.run.connect (run);
		service.start ();
	}

	public bool run (GLib.SocketConnection connection, GLib.Object? source_object) throws Error {
		var address = connection.get_remote_address () as InetSocketAddress;
		var resolver = Resolver.get_default();
		var host = resolver.lookup_by_address (address.get_address ());
		process_request (connection.input_stream, connection.output_stream, host);
		return true;
	}

	public Response? execute_request (Request request) {
		switch (request.resource_id[0]) {
			case "list":
				return new ListExecutor (application_data).execute (request);
			case "status":
				return new StatusExecutor (application_data).execute (request);
			case "get":
				return new FileExecutor (application_data).execute (request);
			case "peers":
				return new PeersExecutor (application_data).execute (request);
		}
		return null;
	}

	void add_method (Request request, DataInputStream data_stream) throws Error {
		string line = data_stream.read_line (null, null);
		string[] string_array = line.split(" ");
		string method = string_array[0].strip();
		string resource = string_array[1].strip();
		string protocol = string_array[2].strip();

		request.method = method;
		request.protocol = protocol;

		foreach (string id in resource.split("/")) {
			if (id != "") {
				request.resource_id.add (id);
			}
		}
	}

	void add_headers (Request request, DataInputStream data_stream) throws Error {
		string line = data_stream.read_line (null, null);
		string[] string_array;
		while (line.strip() != "") {
			print ("'%s'\n", line);
			string_array = line.split (":");
			request.headers[string_array[0].strip()] = string_array[1].strip();
			line = data_stream.read_line (null, null);
		}
	}

	void add_body (Request request, DataInputStream data_stream) throws Error {
		/*if ("Content-Length" in request.headers) {
			int64 content_length = request.headers["Content-Length"].to_int64 ();
		}*/
	}

	bool create_and_validate_request (Request request, InputStream input_stream) throws Error {
		var data_stream = new DataInputStream (input_stream);

		add_method (request, data_stream);
		add_headers (request, data_stream);
		add_body (request, data_stream);

		return true;
	}

	void register_peer (Request request, string host) {
		if ("Local-Server" in request.headers) {
			string package_id = request.resource_id[1];
			var package = application_data.package_list.get_package_for_id (package_id);
			if (package == null) {
				return;
			}
			uint16 port = (uint16) request.headers["Local-Server"].to_uint64 ();
			package.peer_list.add_new_peer (host, port);
		}
	}

	void process_request (InputStream input, OutputStream output, string host) throws Error {
		Request request = new Request();
		bool result = create_and_validate_request (request, input);
		if (result) {
			register_peer (request, host);
			var data_output_stream = new DataOutputStream (output);
			print("Create response..\n");
			Response response = execute_request(request) ?? new FileNotFoundResponse();
			print("Send response header..\n");
			response.write_header (data_output_stream);
			print("Send response content..\n");
			response.write (data_output_stream);
			print("Done\n");
		}
	}
}
